from matplotlib import pyplot
import pylab
from mpl_toolkits.mplot3d import Axes3D
from numpy import array
from numpy import linalg
from numpy import zeros
from numpy import cross
from numpy import dot

class Planet:
    
    def __init__(self, position, velocity, delta_t, sun_mass, plan_mass = 0.003):
        
        for i in range(0,len(position)):
            position[i] = float(position[i])
        
        for i in range(0,len(velocity)):
            velocity[i] = float(velocity[i])
        
        self.u = array(position + velocity) #pos then vel
        self.dt = float(delta_t)
        self.position_log_x = [] #record of x-positions
        self.position_log_y = []
        self.position_log_z = []
        self.sun_m = float(sun_mass) #1.989*(10**30)
	self.plan_m = float(plan_mass)
        self.n_direc = [1,0,0] #unnatural shifting vector
    
    def func(self, vect):
        
        pos = array(vect[0:3])
        vel = array(vect[3:])
        G= 1  #6.673*(10**-11) #grav constant
        accel_coeff = -(G*self.sun_m)/(linalg.norm(pos)**2) 
        
        nvec = pos/(linalg.norm(pos))
        
        accel = accel_coeff*nvec
        
        du = zeros(6)
        du[0:3] = vel
        du[3:] = accel
        n = linalg.norm(du)
                        
        return du/n
        
        
        
    def runge_kutta(self, time_step, vect):
        
        k1 = self.func(vect)
        k2 = self.func(vect + (k1*time_step/2))
        k3 = self.func(vect + (k2*time_step/2))
        k4 = self.func(vect + (k3*time_step))
        
        return vect + (time_step/6)*(k1 + 2*k2 + 2*k3 + k4)


    def update(self):
        
        self.position_log_x += [self.u[0]]
        self.position_log_y += [self.u[1]]
        self.position_log_z += [self.u[2]]
        
        temp_vect = self.runge_kutta(self.dt, self.u)
        self.u = temp_vect
    
    
    def loop(self):
        r0 = array(self.u[0:3])
        self.update()
        r1 = array(self.u[0:3])
        
        vec = cross(r0, r1)
        
        self.update()
        r1 = array(self.u[0:3])
        
        delt = linalg.norm(vec - cross(r0, r1))
        
        
        self.update()
        r1 = array(self.u[0:3])
        
        while (linalg.norm(vec - cross(r0, r1)) > delt) or (dot(r0, r1) < 0) : 
            self.update()
            r1 = array(self.u[0:3])
            if (linalg.norm(earth.u[:3]) < .01) or  (1/(linalg.norm(earth.u[:3])) < .0001):
                 break
            #print linalg.norm(vec - cross(r0, r1))
    
    
    def shift(self):
        
        print self.u
        n = array(self.n_direc)
        nhat = n/linalg.norm(n) #must be normalized
        G= 1  #6.673*(10**-11) #grav constant --NOTE: Make sure this is consistent with G in previous function
        
        pos = array(self.u[0:3])
        p = self.plan_m*array(self.u[3:6])
        
        dr = (1/self.plan_m)*(cross(pos, cross(p,nhat)) - cross(nhat, cross(pos,p)))
        dp = (-1/self.plan_m)*cross(p, cross(nhat, p)) +    G*self.plan_m*self.sun_m*(1/(linalg.norm(pos)**3))*cross(pos, cross(nhat, pos))
        
        temp_shift = zeros(6)
        temp_shift[0:3] = dr
        temp_shift[3:6] = dp/self.plan_m
        
        temp_shift = temp_shift/linalg.norm(temp_shift)
        
        self.u += temp_shift*self.dt
        print self.u
        return


if __name__ == '__main__':
    
    t_int = .1 #time interval
    earth = Planet([25, 10, 25], [3, -1, 2.5], t_int, 1000, .003)
    #earth = Planet([5.29037811, 8.631371,    6.77110987], [7.29120305, 1.89353818, 7.058716], .001, 1000, .003)
    #earth = Planet([1.56677546, 5.47184529, 2.66794253], [9.75160399, 3.65843046, 9.69655601], .001, 1000, .003)
    #earth = Planet([25, 10, 30],[5,2,0], t_int, 1000, .003)
    #earth = Planet([15, 5, 0], [2, 10, 2], t_int, 1000, .003)
    #earth = Planet([324.99460554, 129.99784222, -115.01499542],[-6.45094575, -2.5803783, -17.92774144], t_int, 1000, .003)
    #earth = Planet([25, 0, 0],[0,4,0], t_int, 1000, .003)


    """steps = 50000
    for i in range(0, steps):
        if linalg.norm(earth.u[:3]) < .01:
            steps = i
            break
        earth.update()
        print str((float(i)/steps)*100) + '%'"""
    
    x = [] #list of lists
    y = []
    z = []
    
    for i in range(30):
        earth.loop()
        x += [earth.position_log_x]
        y += [earth.position_log_y]
        z += [earth.position_log_z]
        
        earth.position_log_x = []
        earth.position_log_y = []
        earth.position_log_z = []
        
        for i in range(100):
            earth.shift()
    
    steps = len(earth.position_log_x)
    
    fig = pylab.figure()
    ax = Axes3D(fig)
    
    
    print earth.u #final coordinates and velocity    
    
    for i in range(len(x)):
        ax.plot(x[i], y[i],z[i])
    ax.scatter([0],[0],[0], c='r')
    
    k= 20
    holder = [k, -k]
    ax.scatter(holder, holder, holder)
    pyplot.show()
    
    '''for i in range(0,len(earth.position_log_x)):
        if earth.position_log_x[i] < 0:
            earth.position_log_x[i] *= -1
    
    for i in range(0,len(earth.position_log_y)):
        if earth.position_log_y[i] < 0:
            earth.position_log_y[i] *= -1
        
    for i in range(0,len(earth.position_log_z)):
        if earth.position_log_z[i] < 0:
            earth.position_log_z[i] *= -1
            
    
    print min(earth.position_log_x), min(earth.position_log_y), min(earth.position_log_z)'''
    
