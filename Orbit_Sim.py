from matplotlib import pyplot
import pylab
from mpl_toolkits.mplot3d import Axes3D
from numpy import array
from numpy import linalg

class Planet:
    
    def __init__(self, position, velocity, delta_t, sun_mass):
        
        for i in range(0,len(position)):
            position[i] = float(position[i])
        
        for i in range(0,len(velocity)):
            velocity[i] = float(velocity[i])
        
        self.pos = array(position)
        self.vel = array(velocity)
        #self.acc = array([0,0,0])
        #self.dx = array([0,0,0])
        #self.dv = array([0,0,0])
        self.dt = float(delta_t)
        self.position_log_x = []
        self.position_log_y = []
        self.position_log_z = []
        self.sun_m = float(sun_mass) #1.989*(10**30)



    def runge_kutta(self, time_step, vel_n):
        temp_pos = self.pos + vel_n*time_step
        
        G= 1  #6.673*(10**-11) #grav constant
        accel_coeff = -(G*self.sun_m)/(linalg.norm(temp_pos)**2) 
        
        vector = temp_pos
        nvec = vector/(linalg.norm(vector))
        
        return accel_coeff*nvec


    def update(self):
        #print "vel:" + str(self.vel)
        #print 'pos:' + str(self.pos)
        self.position_log_x += [self.pos[0]]
        self.position_log_y += [self.pos[1]]
        self.position_log_z += [self.pos[2]]
        
        k1 = self.runge_kutta(0, self.vel)
        k2 = self.runge_kutta(self.dt/2, self.vel + (k1*self.dt/2))
        k3 = self.runge_kutta(self.dt/2, self.vel + (k2*self.dt/2))
        k4 = self.runge_kutta(self.dt, self.vel + self.dt*k3)
        #print k1, k2, k3, k4
        
        #print "1:" + str(self.vel)
        self.vel += (self.dt/6)*(k1 + 2*k2 + 2*k3 + k4)
        #print "2:" + str(self.vel)
        self.pos += self.dt*self.vel


if __name__ == '__main__':
    
    t_int = .01 #time interval
    #earth = Planet([25, 10, 25], [3, -1, 2.5], t_int, 1000) #spirals towards center
    #earth = Planet([5.29037811, 8.631371,    6.77110987], [7.29120305, 1.89353818, 7.058716], .001, 1000)
    #earth = Planet([1.56677546, 5.47184529, 2.66794253], [9.75160399, 3.65843046, 9.69655601], .001, 1000)
    earth = Planet([25, 10, 30],[5,2,0], t_int, 1000 )
    #length_follow = []
    steps = 1000000
    for i in range(0, steps):
        if linalg.norm(earth.pos) < .01:
            break
        earth.update()
        print str((float(i)/steps)*100) + '%'
        #length_follow += [linalg.norm(earth.pos)]
    fig = pylab.figure()
    ax = Axes3D(fig)
    
    
    #print length_follow
    plot_points = 500
    if steps < plot_points:
        print 'Your value of plot points is too low!'
    

    v_t_r = int(steps/plot_points) #plotted index steps
    print v_t_r
        
    x = []
    y = []
    z = []
    
    for i in range(0, plot_points - 1):
        
        x += [earth.position_log_x[i*v_t_r]]
        y += [earth.position_log_y[i*v_t_r]]
        z += [earth.position_log_z[i*v_t_r]]
    
    #print x,y,z,
    print "Plotted time step range: " + str(v_t_r*t_int)
    print earth.pos, earth.vel #final coordinates and velocity    
    
    ax.scatter(x,y,z)
    ax.scatter([0],[0],[0], c='r')
    pyplot.show()
    
    '''for i in range(0,len(earth.position_log_x)):
        if earth.position_log_x[i] < 0:
            earth.position_log_x[i] *= -1
    
    for i in range(0,len(earth.position_log_y)):
        if earth.position_log_y[i] < 0:
            earth.position_log_y[i] *= -1
        
    for i in range(0,len(earth.position_log_z)):
        if earth.position_log_z[i] < 0:
            earth.position_log_z[i] *= -1
            
    
    print min(earth.position_log_x), min(earth.position_log_y), min(earth.position_log_z)'''
    