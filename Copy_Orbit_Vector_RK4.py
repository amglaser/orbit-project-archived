from matplotlib import pyplot
import pylab
from mpl_toolkits.mplot3d import Axes3D
from numpy import array
from numpy import linalg
from numpy import zeros
from numpy import dot


class Planet:
    
    def __init__(self, position, velocity, delta_t, sun_mass):
        
        for i in range(0,len(position)):
            position[i] = float(position[i])
        
        for i in range(0,len(velocity)):
            velocity[i] = float(velocity[i])
        
        self.u = array(position + velocity) #pos then vel
        self.dt = float(delta_t)
        self.position_log_x = []
        self.position_log_y = []
        self.position_log_z = []
        self.sun_m = float(sun_mass) #1.989*(10**30)
        self.pos = array(position)
        self.vel = array(velocity) 
    
    def func(self, time_step, vect):
        
        pos = array(vect[0:3])
        vel = array(vect[3:])
        G= 1  #6.673*(10**-11) #grav constant
        accel_coeff = (G*self.sun_m)/(linalg.norm(pos)**2)  
        
        npos = pos/linalg.norm(pos)       
        
        theta = array([-pos[1], pos[0], pos[2]])
        ntheta = theta/linalg.norm(theta)
        
        D = (dot(pos,pos) - pos[2]**2)**(.5)
        phi = array([pos[2]*pos[0]/D, pos[2]*pos[1]/D, -D])
        nphi = phi/linalg.norm(phi)
        
        
        a= -1
        b= .1
        c= 0#.001
        
        vec_direc = a*npos + b*nphi + c*ntheta
        nvec = vec_direc/linalg.norm(vec_direc)
        
        accel = nvec*accel_coeff
        
        du = zeros(6)
        du[0:3] = vel
        du[3:] = accel
        n = linalg.norm(du)
              
        return du/n
        
        
        
    def runge_kutta(self, time_step, vect):
        
        k1 = self.func(time_step, vect)
        k2 = self.func(time_step/2, vect + (k1*time_step/2))
        k3 = self.func(time_step/2, vect + (k2*time_step/2))
        k4 = self.func(time_step, vect + (k3*time_step))
        
        return vect + (time_step/6)*(k1 + 2*k2 + 2*k3 + k4)


    def update(self):
        
        self.position_log_x += [self.u[0]]
        self.position_log_y += [self.u[1]]
        self.position_log_z += [self.u[2]]
        
        temp_vect = self.runge_kutta(self.dt, self.u)
        self.u = temp_vect


if __name__ == '__main__':
    
    t_int = .1 #time interval
    #earth = Planet([25, 10, 25], [3, -1, 2.5], t_int, 1000) #spirals towards center
    #earth = Planet([5.29037811, 8.631371,    6.77110987], [7.29120305, 1.89353818, 7.058716], .001, 1000)
    #earth = Planet([1.56677546, 5.47184529, 2.66794253], [9.75160399, 3.65843046, 9.69655601], .001, 1000)
    #earth = Planet([25, 10, 30],[5,2,0], t_int, 1000 )
    #earth = Planet([15, 5, 0], [2, 10, 2], t_int, 1000)
    earth = Planet([15, 5, 0], [2, 10, 0], t_int, 1000) #WLOG can change coordinates so the plane of the orbit has zhat position and direction of 0
    
    
    steps = 50000
    for i in range(0, steps):
        if linalg.norm(earth.u[:3]) < .01:
            steps = i
            break
        earth.update()
        print str((float(i)/steps)*100) + '%'
    
    fig = pylab.figure()
    ax = Axes3D(fig)
    
    
    plot_points = 500
    if steps < plot_points:
        print 'Your value of plot points is too low!'
    
    if steps < len(earth.position_log_x):
        print 'Your value of steps is too high!'
    

    v_t_r = int(steps/plot_points) #plotted index steps
        
    x = []
    y = []
    z = []
    
    for i in range(0, plot_points - 1):
        
        x += [earth.position_log_x[i*v_t_r]]
        y += [earth.position_log_y[i*v_t_r]]
        z += [earth.position_log_z[i*v_t_r]]
    

    print "Plotted time step range: " + str(v_t_r*t_int)
    print earth.u #final coordinates and velocity    
    
    ax.scatter(x,y,z)
    ax.scatter([0],[0],[0], c='r')
    pyplot.show()
    
    '''for i in range(0,len(earth.position_log_x)):
        if earth.position_log_x[i] < 0:
            earth.position_log_x[i] *= -1
    
    for i in range(0,len(earth.position_log_y)):
        if earth.position_log_y[i] < 0:
            earth.position_log_y[i] *= -1
        
    for i in range(0,len(earth.position_log_z)):
        if earth.position_log_z[i] < 0:
            earth.position_log_z[i] *= -1
            
    
    print min(earth.position_log_x), min(earth.position_log_y), min(earth.position_log_z)'''
    